import React from 'react';
import './App.css';
import NavBar from './components/NavBar'
import Header from './components/Header'
import MainContent from './components/MainContent'
import Footer from './components/Footer'

function App() {
  return (
    <>
      <NavBar />
      <Header />
      <MainContent />
      <Footer />
    </>
  );
}

export default App;
