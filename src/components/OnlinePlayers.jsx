import React, { Component } from "react";

export class OnlinePlayers extends Component {
  render() {
    return (
      <div className="uk-container uk-text-left uk-text-center">
        <p className="uk-text-success">Players online: {this.props.online}</p>
      </div>
    );
  }
}

export default OnlinePlayers;
