import React, { Component } from 'react'
import {
    Accordion,
    AccordionItem,
    Card,
    CardHeader,
    CardTitle,
    CardBody,
} from 'uikit-react'

export class AccordionMain extends Component {
    render() {
        const { playerInfo, playerRank, playerScore, playerStats } = this.props
        return (
            <div hidden={this.props.hidden} playerinfo={this.props.playerInfo}>
                <Accordion options="multiple: true;">
                    <AccordionItem title="Player Info" content={
                        <Card hover className="uk-width-1-2@m">
                            <CardHeader>
                                <CardTitle>
                                    <img className="uk-border-circle" width="50" height="50" alt="dogtags" src="https://images-na.ssl-images-amazon.com/images/I/51GOnGYUylL.jpg" />
                                    <div className="uk-width-expand">
                                        <h3 className="uk-card-title uk-margin-remove-bottom">{playerInfo.name}</h3>
                                        <p className="uk-text-meta uk-margin-remove-top">Rank: {playerRank.nr} {playerRank.name}</p>
                                    </div>
                                </CardTitle>
                                <CardBody>
                                    <p> <span uk-icon="icon: tag"></span>{" "}
                                        <strong>Player score:</strong> {playerScore.totalScore}
                                    </p>
                                    <span uk-icon="icon: clock"></span>{" "}
                                    <strong>Last day online:</strong> {playerInfo.lastDay}
                                </CardBody>
                            </CardHeader>
                        </Card>
                    }>
                    </AccordionItem>
                    <AccordionItem title="Detailed Stats" content={
                        <Card hover className="uk-width-1-2@m">
                            <CardHeader>
                                <CardTitle>
                                    <img className="uk-border-circle" width="50" height="50" alt="dogtags" src="https://images-na.ssl-images-amazon.com/images/I/51GOnGYUylL.jpg" />
                                    <div className="uk-width-expand">
                                        <h3 className="uk-card-title uk-margin-remove-bottom">{playerInfo.name}</h3>
                                        <p className="uk-text-meta uk-margin-remove-top">Rank: {playerRank.nr} {playerRank.name}</p>
                                    </div>
                                </CardTitle>
                                <CardBody>
                                    <div className="uk-grid">
                                        <div>
                                            <p><strong>Kills: </strong> {playerStats.kills}</p>
                                            <p><strong>Deaths: </strong> {playerStats.deaths}</p>
                                            <strong>Kill Assists: </strong> {playerStats.killAssists}
                                        </div>
                                        <div>
                                            <p><strong>Shots fired: </strong> {playerStats.shotsFired}</p>
                                            <p><strong>Shots Hit: </strong> {playerStats.shotsHit} </p>
                                            <p><strong>Headshots: </strong> {playerStats.headshots}</p>
                                        </div>
                                    </div>
                                </CardBody>
                            </CardHeader>
                        </Card>
                    }></AccordionItem>
                    <AccordionItem title="Score Info" content={
                        <Card hover className="uk-width-1-2@m">
                            <CardHeader>
                                <CardTitle>
                                    <img className="uk-border-circle" width="50" height="50" alt="dogtags" src="https://images-na.ssl-images-amazon.com/images/I/51GOnGYUylL.jpg" />
                                    <div className="uk-width-expand">
                                        <h3 className="uk-card-title uk-margin-remove-bottom">{playerInfo.name}</h3>
                                        <p className="uk-text-meta uk-margin-remove-top">Rank: {playerRank.nr} {playerRank.name}</p>
                                    </div>
                                </CardTitle>
                                <CardBody>
                                    <div className="uk-grid">
                                        <div>
                                            <p><strong>Combat Score: </strong> {playerScore.combatScore}</p>
                                            <p><strong>Total Score: </strong> {playerScore.totalScore}</p>
                                            <p><strong>Vehicle Score: </strong> {playerScore.vehicle}</p>
                                        </div>
                                        <div>
                                            <p><strong>Skill: </strong> {playerStats.skill}</p>
                                            <p><strong>Dogtags taken: </strong>{playerStats.dogtagsTaken}</p>
                                            <p><strong>Longest Headshot: </strong>{playerStats.longestHeadshot}</p>
                                        </div>
                                    </div>
                                </CardBody>
                            </CardHeader>
                        </Card>
                    }></AccordionItem>
                </Accordion>
            </div >
        )
    }
}

export default AccordionMain
