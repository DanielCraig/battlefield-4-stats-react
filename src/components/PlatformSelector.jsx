import React, { Component } from "react";
export class PlatformSelector extends Component {
  render() {
    const { handlePlatform } = this.props
    return (
      <div
        uk-form-custom="target: > * > span:first-child"
        className="uk-form-custom"
      >
        <select onChange={handlePlatform}>
          <option> Choose Platform</option>
          <option value="pc">PC</option>
          <option value="xone">Xbox One</option>
          <option value="ps4">PS4</option>
        </select>
        <button className="uk-button uk-button-default" type="button">
          <span>Choose Platform</span>
        </button>
      </div>
    );
  }
}

export default PlatformSelector;
