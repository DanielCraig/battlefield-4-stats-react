import React, { Component } from 'react'
import {
    NavbarContainer,
    Navbar,
    NavbarSticky,
    NavItem,
    Link
} from 'uikit-react'

export class NavBar extends Component {
    render() {
        return (
            <div>
                <NavbarSticky>
                    <NavbarContainer>
                        <Navbar left>
                            <NavItem className="uk-active">
                                <Link href="/">Home</Link>
                            </NavItem>
                        </Navbar>
                    </NavbarContainer>
                </NavbarSticky>
            </div>
        )
    }
}

export default NavBar
