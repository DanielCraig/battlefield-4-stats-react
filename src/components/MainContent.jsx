import React, { Component } from "react";
import SearchBox from "./SearchBox";
import OnlinePlayers from "./OnlinePlayers";
import PlatformSelector from "./PlatformSelector";
import Accordion from "./Accordion"
import { Section, Container, Grid } from "uikit-react";


export default class MainContent extends Component {
  state = {
    onlinePlayers: "",
    isHidden: true,
    username: "",
    platform: "",
    playerInfo: "",
    playerRank: "",
    playerStats: "",
    playerScore: ""
  }

  handlePlatform = (event) => {
    this.setState({
      platform: event.target.value
    })
  }

  handleInput = (event) => {
    this.setState({
      username: event.target.value
    })
  }

  toggleHidden = () => {
    this.setState({
      isHidden: !this.state.isHidden
    })
    const hidden = this.state.isHidden

    if (hidden === true) {
      fetch("https://api.bf4stats.com/api/playerInfo?plat=" + this.state.platform + "&name=" + this.state.username + "&output=json").then(response => response.json()).then(data => {
        this.setState({ playerInfo: data.player, playerRank: data.player.rank, playerStats: data.stats, playerScore: data.stats.scores })
        console.log('Player info:', this.state.playerInfo)
        console.log('Player Rank: ', this.state.playerRank)
        console.log('Player Stats:', this.state.playerStats)
        console.log('Player Score:', this.state.playerScore)
      })
    }
  }


  componentDidMount() {
    fetch("https://api.bf4stats.com/api/onlinePlayers").then(response => response.json()).then(data => {

      let platforms = {
        pc: data.pc.count,
        ps3: data.ps3.count,
        ps4: data.ps4.count,
        xbox360: data.xbox.count,
        xone: data.xone.count
      }

      let total = platforms.pc + platforms.ps3 + platforms.ps4 + platforms.xbox360 + platforms.xone

      this.setState({ onlinePlayers: total })
    })
  }

  render() {
    const { isHidden, onlinePlayers, username, platform, playerInfo, playerRank, playerScore, playerStats } = this.state
    return (
      <div>
        <br />
        <Section>
          <Container>
            <p className="uk-text-lead uk-text-center">
              Search an username, select your platform, and see the magic.
            </p>
          </Container>
        </Section>
        <br />
        <br />
        <Container>
          <Grid gutter=" uk-child-width-expand@s uk-text-center">
            <SearchBox toggleHidden={this.toggleHidden} username={username} handleInput={this.handleInput} />
            <OnlinePlayers online={onlinePlayers} />
            <PlatformSelector platform={platform} handlePlatform={this.handlePlatform} />
          </Grid>
          <br></br>
          <Accordion hidden={isHidden} username={username} platform={platform} playerInfo={playerInfo} playerRank={playerRank} playerScore={playerScore} playerStats={playerStats} />
        </Container>
      </div>
    );
  }
}
