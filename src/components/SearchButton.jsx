import React, { Component } from "react";
import { Button } from "uikit-react";

export class SearchButton extends Component {
  render() {
    return (
      <div>
        <Button type="button" color="text">Search</Button>
      </div>
    );
  }
}

export default SearchButton;
