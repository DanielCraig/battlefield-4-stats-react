import React, { Component } from 'react'

export class Footer extends Component {
    render() {
        const style = {
            backgroundImage: 'url(https://resources.stuff.co.nz/content/dam/images/1/p/x/l/e/v/image.related.StuffLandscapeThreeByTwo.1464x976.1pxfwc.png/1526608657751.jpg)'
        };

        return (
            <div>
                <br></br>
                <footer>
                    <div className="uk-section-default">
                        <div className="uk-section uk-light uk-background-cover" style={style}>
                            <div className="uk-container">
                                <h3>BF4 Stats Fetcher</h3>
                                <div className="uk-grid-match uk-child-width-1-3@m" uk-grid="true">
                                    <div>
                                        <p>Brought to you by <strong>Alessandro Camplese</strong></p>
                                    </div>
                                    <div>
                                        <p>Powered by <strong>UIKit</strong>{" "}<a href="https://getuikit.com/"
                                            className="uk-icon-link uk-margin-small-right" uk-icon="uikit"></a>
                                            and <strong>React.JS</strong>.</p>
                                    </div>
                                    <div>
                                        <p><strong>Follow me on:</strong> </p>
                                        <div className="uk-container">
                                            <a href="https://www.instagram.com/alessandrocampl_qg/"
                                                className="uk-icon-link uk-margin-small-right" uk-icon="instagram"></a>
                                            <a href="https://www.facebook.com/ale.camplese" className="uk-icon-link uk-margin-small-right"
                                                uk-icon="facebook"></a>
                                            <a href="https://www.linkedin.com/in/alessandro-camplese-279b3864/"
                                                className="uk-icon-link uk-margin-small-right" uk-icon="linkedin"></a>
                                            <a href="#" className="uk-icon-link uk-margin-small-right" uk-icon="twitter"></a>
                                            <a href="https://gitlab.com/DanielCraig?nav_source=navbar"
                                                className="uk-icon-link uk-margin-small-right" uk-icon="github"></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        )
    }
}

export default Footer
