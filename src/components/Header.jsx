import React, { Component } from 'react'
import {
    Section,
    Container
} from 'uikit-react'

export class Header extends Component {
    render() {
        const style = {
            backgroundImage: 'url(http://www.gamesvillage.it/wp-content/uploads/2018/05/Battlefield-V-no-loot-box-240518.jpg)',
            backgroundSize: 'auto',
            backgroundRepeat: 'no-repeat',
        }
        return (
            <div>
                <Section>
                    <Container className="uk-text-center uk-section uk-section-secondary" size="expand" style={style}>
                        <h3>Hello.</h3>
                        <h5>Welcome to BF4 stats finder!</h5>
                    </Container>
                </Section>
            </div >
        )
    }
}

export default Header
