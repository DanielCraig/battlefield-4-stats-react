import React, { Component } from "react";

export class SearchBox extends Component {
  render() {
    const { toggleHidden, handleInput } = this.props
    return (
      <div className="uk-first-column">
        <form className="uk-search uk-search-default">
          <input
            onKeyUp={handleInput}
            className="uk-search-input"
            type="text"
            placeholder="Search username.."
          />
          <button onClick={() => toggleHidden()} type="button">Search</button>
        </form>
      </div>
    );
  }
}

export default SearchBox;
